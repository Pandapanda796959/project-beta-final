import { useState, useEffect } from "react";
import { sortTableNumbers } from "../SortFunctions";
import sortTable from "../SortFunctions";
import NavModalSales from "./NavModalSales"

export default function SalesList() {
    const [sales, setSales] = useState([]);
    const [sort, setSort] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [auto, setAuto] = useState({});

    const fetchSales = async () => {
        const url = "http://localhost:8090/api/salerecords/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sale_records);
        }
    }

    const handleSort = (event) => {
        const num = parseInt(event.target.dataset.select)
        sortTable(num)
        if (sort === num) { setSort(false)
          return
        }
        setSort(num)
    }

    const handleNumberSort = (event) => {
        const num = parseInt(event.target.dataset.select)
        sortTableNumbers(num)
        if (sort === num) { setSort(false)
            return
        }
        setSort(num)
    }

    const deleteListing = async (vin) => {
        const url = `http://localhost:8090/api/automobiles/${vin}/`;
        const fetchConfig = {method: "delete"};
        await fetch(url, fetchConfig);
        fetchSales();
    }

    var getAutoData = async (vin) => {
        const response = await fetch(
          `http://localhost:8100/api/automobiles/${vin}`
        );
        if (response.ok) {
          const data = await response.json();
          setAuto(data);
        }
      };

    function handleModal(e) {
        getAutoData(e.target.value);
        setOpenModal(!openModal);
        setAuto({});
      }

      function handleCloseModal() {
        setOpenModal(false);
        setAuto({});
      }

    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <div className="bg-white bg-opacity-75 mt-3 rounded-md">
            <h1 className="text-2xl">Sales</h1>
            <div>
                <table className="table table-striped table-sortable" id="myTable">
                    <caption />
                    <thead>
                        <tr>
                            <th className={sort === 0 ? 'asc' : 'desc'} data-select="0" onClick={ handleSort }>Salesperson</th>
                            <th className={sort === 1 ? 'asc' : 'desc'} data-select="1" onClick={ handleSort }>Customer</th>
                            <th className={sort === 2 ? 'asc' : 'desc'} data-select="2" onClick={ handleSort }>Vehicle VIN</th>
                            <th className={sort === 3 ? 'asc' : 'desc'} data-select="3" onClick={ handleNumberSort }>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map(sale => {
                            return(
                                <tr key={sale.id}>
                                    <td>{sale.salesperson.name}</td>
                                    <td>{sale.customer.name}</td>
                                    <td>
                                        <button
                                            type="button"
                                            className="btn btn-link"
                                            value={sale.automobile.vin}
                                            onClick={handleModal}
                                        >
                                            {sale.automobile.vin}
                                        </button>
                                        {openModal ? (
                                            <NavModalSales closeModal={handleCloseModal} auto={auto} sale={sale} />
                                        ) : (
                                            <></>
                                        )}
                                    </td>
                                    <td value={sale.price}>{sale.price.toLocaleString("en-US",{style:"currency",currency:"USD"})}</td>
                                    <td><button className="btn btn-info" onClick={() => deleteListing(sale.automobile.vin)}>Delete</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
