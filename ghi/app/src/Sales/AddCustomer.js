import { useState, useEffect } from "react";

export default function AddCustomer() {
    const [addressForm, setAddressForm] = useState({
        street_address: "",
        city: "",
        state: "",
        zip: ""
    });
    const [formData, setFormData] = useState({
        name: "",
        address: "",
        phone_number: ""
    });

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/customers/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok){
            setFormData({
                name: "",
                address: "",
                phone_number: ""
            });
            setAddressForm({
                street_address: "",
                city: "",
                state: "",
                zip: ""
            });
        }
    }

    const stringifyAddress = () => {
        return(
            `${addressForm.street_address} ${addressForm.city}, ${addressForm.state} ${addressForm.zip}`
        );
    }

    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setFormData({
            ...formData,
            [name]: value
        });
    }

    const handleAddressChange = (event) => {
        event.preventDefault();
        const name = event.target.name;
        const value = event.target.value;

        setAddressForm({
            ...addressForm,
            [name]: value
        });
    }

    const handleClick = () => {
        setFormData({
            ...formData,
            ["address"]: stringifyAddress()
        });
    }

    return(
        <div >
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{ width: 575}} >
            <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
                <h1 className="text-white text-center text-2xl">Add a potential customer</h1>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="name" type="text" value={formData.name} placeholder="Name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-group col-5 mx-auto mt-3" style={{width: 300}}>
                    <div className="form-floating">
                        <input onChange={handleAddressChange} name="street_address" type="text" placeholder="Street address" value={addressForm.street_address} className="form-control" />
                        <label htmlFor="street_address">Street address</label>
                    </div>
                    <div className="input-group">
                        <div className=" form-floating" style={{width: 165}}>
                            <input onChange={handleAddressChange} name="city" type="text" placeholder="City" value={addressForm.city} className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className=" form-floating" style={{width: 60}}>
                            <input onChange={handleAddressChange}  name="state" type="text" placeholder="State" value={addressForm.state} className="form-control" />
                            <label htmlFor="state">State</label>
                        </div>
                        <div className=" form-floating" style={{width: 77}}>
                            <input onChange={handleAddressChange} name="zip" type="text" placeholder="Zip" value={addressForm.zip} className="form-control" />
                            <label htmlFor="zip">Zip</label>
                        </div>
                    </div>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="phone_number" type="text" value={formData.phone_number} placeholder="Phone number" className="form-control" />
                    <label htmlFor="phone_number">Phone number</label>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{width: 300}}>
                    <button onClick={handleClick} className="btn bg-blue hover:bg-gray btn-block mt-3">Add</button>
                </div>
            </form>
        </div>
        </div>
    );
}
