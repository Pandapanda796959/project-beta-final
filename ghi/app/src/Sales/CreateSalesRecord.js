import { useState, useEffect } from "react"

export default function CreateSalesRecord(){
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([])
    const [formData, setFormData] = useState ({
        salesperson: "",
        customer: "",
        automobile: "",
        price: ""
    })

    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespersons/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons);
        }
    }

    const fetchCustomers = async() => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    const fetchAutomobiles = async() => {
        const url = "http://localhost:8090/api/automobiles";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.auto_vos);
        }
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/salerecords/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok){
            updateInventory(formData.automobile);
            setFormData({
                salesperson: "",
                customer: "",
                automobile: "",
                price: 0
            });
        }
    }

    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setFormData({
            ...formData,
            [name]: value
        });
    }

    const updateInventory = async (vin) => {
        const url = `http://localhost:8090/api/automobiles/${vin}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({"sold": true}),
            headers: {
                "Content-Type": "application/json"
            }
        }
        await fetch(url, fetchConfig);
    }

    useEffect(() => {
        fetchSalespeople();
        fetchCustomers();
        fetchAutomobiles();
    }, [])

    return(
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 550}} >
            <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
                <h1 className="text-white text-center text-2xl">Record a sale</h1>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group col-5 mx-auto mt-2" style={{width: 300}}>
                    <label htmlFor="salesperson">Salesperson</label>
                    <select onChange={handleFormChange} required name="salesperson" value={formData.salesperson} className="form-control">
                        <option value="">Choose a salesperson</option>
                        {salespeople.map(salesperson => {
                            return(
                                <option key={salesperson.id} value={salesperson.id}>{salesperson.name}</option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-group col-5 mx-auto mt-2" style={{width: 300}}>
                    <label htmlFor="customer">Customer</label>
                    <select onChange={handleFormChange} required name="customer" value={formData.customer} className="form-control">
                        <option value="">Choose a customer</option>
                        {customers.map(customer => {
                            return(
                                <option key={customer.id} value={customer.id}>{customer.name}</option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-group col-5 mx-auto mt-2" style={{width: 300}}>
                    <label htmlFor="vin">VIN</label>
                    <select onChange={handleFormChange} required name="automobile" value={formData.automobile} className="form-control">
                        <option value="">Choose a VIN</option>
                        {automobiles
                            .filter(automobile => automobile.sold == false)
                            .map(automobile => {
                                return(
                                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                );
                            })}
                    </select>
                </div>
                <div className="form-group col-5 mx-auto mt-2" style={{width: 300}}>
                    <label htmlFor="price">Price</label>
                    <div className="input-group">
                        <span className="input-group-text">$</span>
                        <input onChange={handleFormChange} required name="price" type="number" value={formData.price} className="form-control" />
                        <span className="input-group-text">.00</span>
                    </div>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{width: 300}}>
                    <button className="btn bg-blue hover:bg-gray btn-block mt-3">Record</button>
                </div>
            </form>
        </div>
    );
}
