import { useState, useEffect } from "react";

export default function AddAutomobile() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: "",
        year: "",
        vin: "",
        model_id: 0
    });

    const fetchModels = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8100/api/automobiles/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                color: "",
                year: "",
                vin: "",
                model_id: 0
            });
        }
    }

    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setFormData({
            ...formData, [name]: value
        });
    }

    useEffect(() => {
        fetchModels();
    }, []);

    return(
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 500}}>
            <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
                <h1 className="text-white text-center text-2xl">Add an automobile</h1>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="color" type="text" value={formData.color} placeholder="Color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} name="year" type="text" value={formData.year} placeholder="Year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} name="vin" type="text" value={formData.vin} placeholder="VIN" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-group col-5 mx-auto mt-3" style={{width: 300}}>
                    <label htmlFor="model_id">Model</label>
                    <select onChange={handleFormChange} required name="model_id" value={formData.model_id} className="form-control">
                        <option value="">Choose a model</option>
                        {models.map(model => {
                            return(
                                <option key={model.id} value={model.id}>{model.name}</option>
                            );
                        })}
                    </select>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{width: 300}}>
                    <button className="btn bg-blue hover:bg-gray btn-block mt-3">Add</button>
                </div>
            </form>
        </div>
    );
}
