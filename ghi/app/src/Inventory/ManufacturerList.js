import { useState, useEffect } from "react";
import sortTable from '../SortFunctions';

export default function ManufacturerList() {
    const [sort, setSort] = useState(false);
    const [manufacturers, setManufacturers] = useState([]);

    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const handleSort = (event) => {
      const num = parseInt(event.target.dataset.select)
      sortTable(num)
      if (sort === num) { setSort(false)
        return
      }
      setSort(num)
    }

    useEffect( () => {
        fetchManufacturers();
    }, []);

    return(
        <div className="bg-white bg-opacity-75 col col-sm-auto mt-3 rounded-md">
            <h1 className="text-2xl">Manufacturers</h1>
            <table className="table table-striped table-sortable" id="myTable">
                <caption />
                <thead>
                    <tr>
                        <th className={sort === 0 ? 'asc' : 'desc'} data-select="0" onClick={ handleSort }>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return(
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
