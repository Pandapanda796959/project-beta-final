import { useState, useEffect } from "react";
import sortTable from '../SortFunctions';

export default function ModelsList() {
    const [models, setModels] = useState([]);
    const [sort, setSort] = useState(false);

    const fetchModels = async () => {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect( () => {
        fetchModels();
    }, []);

    const handleSort = (event) => {
      const num = parseInt(event.target.dataset.select)
      sortTable(num)
      if (sort === num) { setSort(false)
        return
      }
      setSort(num)
    }

    return(
        <div className="bg-white bg-opacity-75 col col-sm-auto mt-3 rounded-md">
            <h1 className="text-2xl">Car Models</h1>
            <table className="table table-striped table-sortable" id="myTable">
                <caption />
                <thead>
                    <tr>
                        <th width="20%" className={sort === 0 ? 'asc' : 'desc'} data-select="0" onClick={ handleSort }>Name</th>
                        <th width="20%" className={sort === 1 ? 'asc' : 'desc'} data-select="1" onClick={ handleSort }>Manufacturer</th>
                        <th width="40%">Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key={model.id}>
                                <td className="align-middle">{model.name}</td>
                                <td className="align-middle">{model.manufacturer.name}</td>
                                <td>
                                    <img src={model.picture_url} className="img-fluid" width="40%" />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
