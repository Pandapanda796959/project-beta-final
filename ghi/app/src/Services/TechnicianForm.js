import React, {useState} from 'react'

function TechnicianForm() {
    const [formData, setFormData] = useState({
        name: '',
        employee_number: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8080/api/technician/'

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                employee_number: '',
            })
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return(
      <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 500}}>
        <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
          <h1 className="text-center text-white text-2xl">Add a Technician</h1>
        </div>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mt-3 mx-auto" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Full Name</label>
            </div>
            <div className="form-floating mt-3 mx-auto" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.employee_number} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
              <label htmlFor="employee_number">Employee Number</label>
            </div>
            <div className="d-grid form group col-7 mx-auto">
              <button className="btn bg-blue hover:bg-gray btn-block mt-3">Add a Technician</button>
            </div>
          </form>
        </div>
    </div>
    )
}

export default TechnicianForm
