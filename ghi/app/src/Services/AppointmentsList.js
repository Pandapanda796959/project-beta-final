import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import { sortTable, sortTableDates } from "../SortFunctions";
import NavModal from "./NavModal";

function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);
  const [sort, setSort] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [auto, setAuto] = useState({});
  const [activeVin, setActiveVin] = useState("");

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const cancelAppointment = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment}/`;
    const fetchConfig = { method: "delete" };
    await fetch(url, fetchConfig);
    getData();
  };

  const filterFinished = () => {
    return appointments.filter((appointment) => appointment.finished === false);
  };

  const fetchVipStatus = (appointment) => {
    if (appointment.vip === true) {
      return "Yes";
    } else {
      return "No";
    }
  };

  const markFinished = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ finished: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    await fetch(url, fetchConfig);
    getData();
  };

  const handleSort = (event) => {
    const num = parseInt(event.target.dataset.select);
    sortTable(num);
    if (sort === num) {
      setSort(false);
      return;
    }
    setSort(num);
  };

  const handleDateSort = (event) => {
    const num = parseInt(event.target.dataset.select);
    sortTableDates(num);
    if (sort === num) {
      setSort(false);
      return;
    }
    setSort(num);
  };

  var getAutoData = async (vin) => {
    const response = await fetch(
      `http://localhost:8100/api/automobiles/${vin}`
    );
    if (response.ok) {
      const data = await response.json();
      setAuto(data);
    }
  };

  function handleModal(e) {
    setActiveVin(e.target.value);
    getAutoData(e.target.value);
    setOpenModal(!openModal);
    setAuto({});
  }

  function handleCloseModal() {
    setOpenModal(false);
    setAuto({});
  }

  return (
    <div className="bg-white bg-opacity-75 rounded-md">
      <div className="col col-sm-auto mt-3">
        <h1 className="text-2xl">Appointments List</h1>
      </div>
      <table className="table table-striped table-sortable" id="myTable">
        <caption />
        <thead>
          <tr>
            <th
              className={sort === 0 ? "asc" : "desc"}
              data-select="0"
              onClick={handleSort}
            >
              VIN
            </th>
            <th
              className={sort === 1 ? "asc" : "desc"}
              data-select="1"
              onClick={handleSort}
            >
              Customer Name
            </th>
            <th
              className={sort === 2 ? "asc" : "desc"}
              data-select="2"
              onClick={handleDateSort}
            >
              Date
            </th>
            <th
              className={sort === 3 ? "asc" : "desc"}
              data-select="3"
              onClick={handleDateSort}
            >
              Time
            </th>
            <th
              className={sort === 4 ? "asc" : "desc"}
              data-select="4"
              onClick={handleSort}
            >
              Technician
            </th>
            <th
              className={sort === 5 ? "asc" : "desc"}
              data-select="5"
              onClick={handleSort}
            >
              VIP Customer
            </th>
            <th
              className={sort === 6 ? "asc" : "desc"}
              data-select="6"
              onClick={handleSort}
            >
              Reason for Visit
            </th>
          </tr>
        </thead>
        <tbody>
          {filterFinished().map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>
                  <button
                    type="button"
                    className="btn btn-link"
                    value={appointment.vin}
                    onClick={handleModal}
                  >
                    {appointment.vin}
                  </button>
                  {openModal ? (
                    <NavModal
                      closeModal={handleCloseModal}
                      auto={auto}
                      appointment={appointment}
                      activevin={activeVin}
                    />
                  ) : (
                    <></>
                  )}
                </td>
                <td>{appointment.customer_name}</td>
                <td value={appointment.date}>
                  {dayjs(appointment.date).format("ddd MMM D, YYYY")}
                </td>
                <td value={appointment.time}>
                  {dayjs(appointment.date + appointment.time).format("hh:mm A")}
                </td>
                <td>{appointment.technician.name}</td>
                <td>{fetchVipStatus(appointment)}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button
                    className="btn bg-blue btn-block"
                    onClick={() => cancelAppointment(appointment.id)}
                  >
                    Cancel
                  </button>
                </td>
                <td>
                  <button
                    className="btn bg-blue btn-block"
                    onClick={() => markFinished(appointment.id)}
                  >
                    Finished
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentsList;
