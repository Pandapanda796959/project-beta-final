import React, {useState, useEffect} from 'react'

function AppointmentForm() {
    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        customer_name: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technician/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technician)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8080/api/appointments/'

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setFormData({
                vin: '',
                customer_name: '',
                date: '',
                time: '',
                technician: '',
                reason: '',
            })
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return(
      <>
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 550}}>
          <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
            <h1 className="text-center text-white text-2xl">Make an Appointment</h1>
          </div>
          <form onSubmit={handleSubmit} className="mx-auto">
            <div className="form-floating mx-auto mt-3" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Vehicle Identification Number (VIN)</label>
            </div>
            <div className="form-floating mx-auto mt-3" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.customer_name} placeholder="Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
              <label htmlFor="customer_name">Full Name</label>
            </div>
            <div className="form-floating mx-auto mt-3" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.date} placeholder="Date" required type="date" name="date" id="date" className="form-control"  />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mx-auto mt-3" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.time} placeholder="Time" required type="time" name="time" id="time" className="form-control"  />
              <label htmlFor="time">Time</label>
            </div>
            <div className="mx-auto mt-3" style={{width: 300}}>
              <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician"  className="form-select">
                <option value="">Choose a Technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.name}>{technician.name}</option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mx-auto mt-3" style={{width: 300}}>
              <input onChange={handleFormChange} value={formData.reason} placeholder="Reason for Appointment" required type="text" id="reason" name="reason" className="form-control"  />
              <label htmlFor="reason">Reason...</label>
            </div>
            <div className="d-grid form group col-7 mx-auto">
              <button className="btn bg-blue hover:bg-gray btn-block mt-3">Make Appointment</button>
            </div>
          </form>
        </div>
      </>
    )
}

export default AppointmentForm
