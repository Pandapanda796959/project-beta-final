import { NavLink } from 'react-router-dom';
import { WrenchIcon, CurrencyDollarIcon, ClipboardDocumentIcon } from '@heroicons/react/24/solid';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-gradient-to-l from-blue to-gray-dark nav-sticky">
      <div className="container-fluid">
        <div id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="mt-3">
              <NavLink className="navbar-brand text-2xl font-['Orbitron']" to="/">CarCar</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="" role="button" data-bs-toggle="dropdown" aria-expanded="false"><ClipboardDocumentIcon className="h-6 w-6 mx-auto"/>Inventory</NavLink>
              <ul className="dropdown-menu border-x-gray-dark rounded-b-md">
                <li><NavLink className="dropdown-item" to="inventory/manufacturers/">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="inventory/manufacturers/add/">Add a manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="inventory/models/">Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="inventory/models/add/">Add a model</NavLink></li>
                <li><NavLink className="dropdown-item" to="inventory/automobiles/">Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="inventory/automobiles/add/">Add an automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="" role="button" data-bs-toggle="dropdown" aria-expanded="false"><WrenchIcon className="h-6 w-6 mx-auto"/>Services</NavLink>
                  <ul className="dropdown-menu border-x-gray-dark rounded-b-md">
                    <li><NavLink className="dropdown-item" to="services/appointments/">Appointments List</NavLink></li>
                    <li><NavLink className="dropdown-item" to="services/appointments/history/">Appointments History</NavLink></li>
                    <li><NavLink className="dropdown-item" to="services/appointments/new/">Add an appointment</NavLink></li>
                    <li><NavLink className="dropdown-item" to="services/technician/">Add a technician</NavLink></li>
                  </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="" role="button" data-bs-toggle="dropdown" aria-expanded="false"><CurrencyDollarIcon className="h-6 w-6 mx-auto"/>Sales</NavLink>
              <ul className="dropdown-menu border-x-gray-dark rounded-b-md">
                <li><NavLink className="dropdown-item" to="sales/">Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="sales/salespeople/">Salesperson history</NavLink></li>
                <li><NavLink className="dropdown-item" to="sales/create/">Record a sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="sales/salespeople/add/">Add a salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="sales/customers/add/">Add a customer</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
