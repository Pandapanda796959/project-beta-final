from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutomobileVO, Appointments, Technician
from .encoders import AutoEncoder, AppointmentsEncoder, TechnicianEncoder


@require_http_methods(["GET"])
def api_automobiles_list(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse({"autos": autos}, encoder=AutoEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointments.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentsEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        autos = AutomobileVO.objects.all()
        for auto in autos:
            if content["vin"] in auto.vin:
                content["vip"] = True
        try:
            tech = Technician.objects.get(name=content["technician"])
            content["technician"] = tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "The specified Technician does NOT exist"}, status=400
            )
        appointment = Appointments.objects.create(**content)
        return JsonResponse(
            {"appointment": appointment}, encoder=AppointmentsEncoder, safe=False
        )


@require_http_methods(["DELETE", "PUT"])
def api_delete_appointments(request, id):
    if request.method == "DELETE":
        count, _ = Appointments.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0 })
    else:
        content = json.loads(request.body)
        Appointments.objects.filter(id=id).update(**content)
        appointment = Appointments.objects.filter(id=id)
        return JsonResponse({"appointment": appointment}, encoder=AppointmentsEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician}, encoder=TechnicianEncoder, safe=False
        )
    if request.method == "POST":
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse({"technician": tech}, encoder=TechnicianEncoder, safe=False)


@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0 })
