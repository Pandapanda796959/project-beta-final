from django.urls import path
from .views import api_automobiles_list, api_list_appointments, api_delete_technician, api_delete_appointments, api_technician


urlpatterns = [
    path("automobiles/", api_automobiles_list, name="api_automobiles_list"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_delete_appointments, name="api_delete_appointments"),
    path("technician/", api_technician, name="api_technician"),
    path("technician/<int:id>/", api_delete_technician, name="api_delete_technician"),
]
