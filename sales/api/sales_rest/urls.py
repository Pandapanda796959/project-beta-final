from django.urls import path
from .views import api_view_salespersons, api_delete_salesperson, \
api_view_customers, api_delete_customer, api_view_sale_records, \
api_delete_sale_record, api_get_automobile_vo, api_edit_automobile_vo


urlpatterns = [
    path("salespersons/", api_view_salespersons),
    path("salespersons/<int:id>/", api_delete_salesperson),
    path("customers/", api_view_customers),
    path("customers/<int:id>/", api_delete_customer),
    path("salerecords/", api_view_sale_records),
    path("salerecords/<int:id>/", api_delete_sale_record),
    path("automobiles/", api_get_automobile_vo),
    path("automobiles/<str:vin>/", api_edit_automobile_vo)
]
