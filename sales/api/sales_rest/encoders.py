from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, SaleRecord


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "name",
        "employee_number"
    ]


class CustomerEncoder(ModelEncoder):
    model= Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model=AutomobileVO
    properties = [
        "import_href",
        "sold",
        "color",
        "year",
        "vin",
        "model",
        "manufacturer",
        "picture_URL"
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "price",
        "automobile",
        "salesperson",
        "customer"
    ]
    encoders = {"salesperson": SalespersonEncoder(),
                "customer": CustomerEncoder(),
                "automobile": AutomobileVOEncoder()}
