from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import SalespersonEncoder, CustomerEncoder, SaleRecordEncoder, AutomobileVOEncoder
from .models import SaleRecord, Customer, Salesperson, AutomobileVO
import json


@require_http_methods(["GET", "POST"])
def api_view_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder = SalespersonEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    count, _ = Salesperson.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_view_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder = CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    count, _ = Customer.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_view_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": sale_records},
            encoder = SaleRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "invalid salesperson id"}, status=400)

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "invalid customer id"}, status=400)

        try:
            automobile_vo_id = content["automobile"]
            automobile_vo = AutomobileVO.objects.get(vin=automobile_vo_id)
            content["automobile"] = automobile_vo
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "invalid automobile vin"}, status=400)

        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_sale_record(request, id):
    count, _ = SaleRecord.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT", "DELETE"])
def api_edit_automobile_vo(request, vin):
    if request.method == "PUT":
        content = json.loads(request.body)
        auto = AutomobileVO.objects.filter(vin=vin).update(sold=content["sold"])
        return JsonResponse(
            {"auto_vo": auto},
            encoder=AutomobileVOEncoder,
            safe=False
        )
    else:
        count, _ = AutomobileVO.objects.filter(vin=vin).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_get_automobile_vo(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse(
        {"auto_vos": autos},
        encoder=AutomobileVOEncoder,
        safe=False
    )
